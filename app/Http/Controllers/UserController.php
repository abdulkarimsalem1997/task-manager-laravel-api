<?php

namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{

    public function index()
    {
        return User::all();
    }

    
    public function show($id)
    {
        return User::findOrFail($id);
    }


    public function tasks()
    {
        $tasks = Auth::user()->tasks;
        $tasksWithProjects = $tasks->map(function ($task) {
            $task['project'] = $task->project->first();
            return $task;
        });
        return response()->json($tasksWithProjects);
    }

    public function projects()
    {
        $projects = auth()->user()->projects;
        $projectsWithAdmins = $projects->map(function ($project) {
            $project['admin'] = $project->admin()->first();
            return $project;
        });

        return $projectsWithAdmins;
    }
}
