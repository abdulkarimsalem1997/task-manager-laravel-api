<?php

namespace App\Http\Controllers;

use Spatie\Permission\Models\Role;
use App\Models\User;
use App\Models\Project;
use App\Models\UserProject;
use Illuminate\Auth\Events\Failed;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Mail\ProjectInvitation;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use App\Rules\IsUserInProject;
use Illuminate\Support\Facades\DB;

use Exception;

class ProjectController extends Controller
{

    public function __construct()
    {
        // Routes that admin and super_user only access
        $this->middleware('project_role:admin')->only(['update', 'destroy']);
        $this->middleware('project_role:super_member,admin')->only(['inviteUser','removeUser', 'updateUserRole']);
    }

    public function index()
    {
        return response()->json([
            'prjects' =>  Project::all(),
            'msg' => 'Get all prjects successfully'
        ]);
        
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|string|max:255',
            'description' => 'nullable|string',
        ]);
      
        // if (!auth()->user()->hasRole('admin')) {
        //     abort(403);
        // }


        $project = Project::create([
            'name' => $validatedData['name'],
            'description' => $validatedData['description'],
        ]);

        $user = auth()->user();
        $project->assignRole($user, 'admin');

        return response()->json($project, 201);
    }

    public function show($id)
    {
        try {
            
            $project = Project::findOrFail($id);
 
        } catch (ModelNotFoundException $e) {
            
            return response()->json(['error' => 'Project not found'], 404);

        } catch (Exception $e) {
          
            return response()->json(['error' => 'An unexpected error occurred'], 500);
        }
       

        return response()->json($project);
    }

    public function showUsersInProject($projectId){
        try {
            $project = Project::findOrFail($projectId);
        } catch (\Throwable $th) {
            return response()->json(['error' => 'project not found']);
        }
        return response()->json($project->users);
    }

    public function update(Request $request, $id)
    {
        
        try {
            $project = Project::findOrFail($id);
 
        } catch (ModelNotFoundException $e) {
            
            return response()->json(['error' => 'Project not found to updated'], 404);

        } catch (Exception $e) {
          
            return response()->json(['error' => 'An unexpected error occurred'], 500);
        }
       

        $project->update($request->all());

        return response()->json($project);
    }

    //  update user role in a project
    public function updateUserRole(Request $request, $projectId, $userId)
    {
        $validate = Validator::make(array_merge($request->all(), ['project_id' => $projectId, 'user_id' => $userId]), [
            'role' => 'required|in:super_member,member',
            'project_id' => 'required|exists:projects,id',
            'user_id' => ['required', 'exists:users,id',
             new IsUserInProject($projectId)],
        ]);
       
        if ($validate->fails()) {
            return response()->json($validate->errors());
        }
       
        $currentUser = Auth::user();
        $currentUserRole = DB::table('user_project')
            ->where('user_id', $currentUser->id)
            ->where('project_id', $projectId)
            ->first()
            ->role_id;

        $targetUserRole = DB::table('user_project')
            ->where('user_id', $userId)
            ->where('project_id', $projectId)
            ->first()
            ->role_id;

        $adminRoleId = Role::where('name', 'admin')->first()->id;
        $superMemberRoleId = Role::where('name', 'super_member')->first()->id;

        // Check permissions
        if ($currentUserRole == $superMemberRoleId && ($targetUserRole == $adminRoleId || $request->role == 'admin')) {
            return response()->json(['error' => 'cannot change'], 403);
        }

        if ($currentUserRole == $superMemberRoleId && $targetUserRole == $superMemberRoleId && $request->role == 'member') {
            return response()->json(['error' => 'cannot change'], 403);
        }
        $project = Project::find($projectId);
        $project->assignRole($userId, $request->role);

        return response()->json(['message' => 'User role updated successfully']);
    }

    //  Remove a user from a project
    public function removeUser($projectId, $userId)
    {
        $validate = Validator::make(array_merge(['project_id' => $projectId, 'user_id' => $userId]),
         [
            'project_id' => 'required|exists:projects,id',
            'user_id' => ['required', 'exists:users,id',
             new IsUserInProject($projectId)],
        ]);

        if ($validate->fails()) {
            return response()->json($validate->errors());
        }


        $currentUser = Auth::user();
        $currentUserRole = DB::table('user_project')
            ->where('user_id', $currentUser->id)
            ->where('project_id', $projectId)
            ->first()
            ->role_id;

        $targetUserRole = DB::table('user_project')
            ->where('user_id', $userId)
            ->where('project_id', $projectId)
            ->first()
            ->role_id;

        $adminRoleId = Role::where('name', 'admin')->first()->id;
        $superMemberRoleId = Role::where('name', 'super_member')->first()->id;

        // Check permissions
        if ($targetUserRole == $adminRoleId) {
            return response()->json(['error' => 'Cannot remove admin from project'], 403);
        }

        if ($currentUserRole == $superMemberRoleId && $targetUserRole == $superMemberRoleId) {
            return response()->json(['error' => 'super member cannot remove another super member'], 403);
        }
        $project = Project::find($projectId);
        $project->users()->detach($userId);

        return response()->json(['message' => 'User removed from project successfully']);
    }

    public function destroy($id)
    {

        try {
            $project = Project::findOrFail($id);

        } catch (ModelNotFoundException $e) {
            
            return response()->json(['error' => 'Project not found to deleted'], 404);

        } catch (Exception $e) {
          
            return response()->json(['error' => 'An unexpected error occurred'], 500);
        }

        
        $project->delete();

        return response()->json(null, 204);
    }
    // for send invent email 

    public function inviteUser(Request $request, $id)
    {
   
        $request->validate([
            'email' => 'required|email|exists:users,email',
            'role' => 'required|in:member,super_member',
            
        ]);
        $project = Project::findOrFail($id);
        $inviter = auth()->user();
        $invitee = User::where('email', $request->email)->first();

         //check if user already exist in project
         $userAlreadyExist = $project->users()->where('user_id', $invitee->id)->exists();
        //  if ($userAlreadyExist) {
        //      return response()->json(['message' => 'user already invited']);
        //  } else {
        
        $role = Role::where('name', $request->role)->first();
        if (!$role) {
            return response()->json(['message' => 'Invalid role'], 400);
        }

        // send email invitation
        Mail::to($invitee->email)->send(new ProjectInvitation($project, $inviter, $invitee , $request->role));
        $invitee->projects()->attach($project->id, ['role_id' => $role->id, 'status' => 'invited']);

        return response()->json([
            "data" => "Invitation sent successfully"
            
        ], 200);  
    // }
}

    public function acceptInvite($projectId, $inviteeId)
    {
     
        $project = Project::findOrFail($projectId);
        $invitee = User::findOrFail($inviteeId);
    
        $project->users()->updateExistingPivot($invitee->id, ['status' => 'accepted']);
    
        return response()->json(['message' => 'Invitation accepted successfully'], 200);
        
    }
}
