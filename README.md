# Task Manager API

Task Manager is a api built with Laravel 10 to manage and track tasks and projects efficiently.

## Features

- User authentication
- Project management
- Task management
- Comment management

## Technologies Used

- Laravel Framework
- MySQL Database


### Installation

1. Clone the repository:
    ```sh
    git clone https://gitlab.com/abdulkarimsalem1997/task-manager-laravel-api.git
    cd task-management-api
    ```

2. Install dependencies:
    ```sh
    composer install
    ```

3. Copy `.env.example` to `.env` and configure your environment variables:
    ```sh
    cp .env.example .env
    ```

4. Generate application key:
    ```sh
    php artisan key:generate
    ```

5. Run migrations and seed the database:
    ```sh
    php artisan migrate --seed
    ```

6. Start the development server:
    ```sh
    php artisan serve
    ```

    ### Using Postman Collection

1. Import the Postman Collection:
    - Open Postman.
    - Click on `Import` in the top left corner.
    - Select the `Task Manager API.postman_collection` file from the project root.