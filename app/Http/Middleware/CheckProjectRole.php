<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Auth;    


class CheckProjectRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next, ...$roles): Response
    {
        if (!Auth::check()) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        $user = Auth::user();
        $projectId = $request->route('project_id');

        // Get the user_project record for the current user and specified project
        $userProject = DB::table('user_project')
            ->where('user_id', $user->id)
            ->where('project_id', $projectId)
            ->first();

        if (!$userProject) {
            return response()->json(['error' => 'Not Allowed'], 403);
        }

        // Get role_id from the user_project record
        $roleId = $userProject->role_id;

        // Retrieve role name from the roles table using role_id
        $roleName = Role::findOrFail($roleId)->name;

        // Check if the role is allowed
        if (!in_array($roleName, $roles)) {
            return response()->json(['error' => 'Not Allowed'], 403);
        }

        return $next($request);
    }
}
