<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;
use Spatie\Permission\Models\Role;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void

    {
        $this->call([
            RolesAndPermissionsSeeder::class,
            UserSeeder::class,
            ProjectSeeder::class,
            UserProjectSeeder::class,
            TaskSeeder::class,
            TaskCommentSeeder::class,
            ProjectCommentSeeder::class,
        ]);

        // create inial user   
        $admin = User::create([
            'name' => 'Admin',
            'email' => 'admin@example.com',
            'password' => bcrypt('password'),
        ]);

        // Create the "admin" role if it does not exist
         Role::firstOrCreate(['name' => 'admin']);

        // Assign the role to the user
        $admin->assignRole('admin');
    }
}
