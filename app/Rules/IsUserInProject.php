<?php

namespace App\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Support\Facades\DB;

class IsUserInProject implements ValidationRule
{
    protected $projectId;

    public function __construct($projectId)
    {
        $this->projectId = $projectId;
    }

    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        if (!DB::table('user_project')
                ->where('project_id', $this->projectId)
                ->where('user_id', $value)
                ->exists()) {
            $fail('User is not part of this project.');
        }
    }
}
