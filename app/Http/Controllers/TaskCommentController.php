<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TaskComment;
use App\Models\Task;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class TaskCommentController extends Controller
{
    public function index($taskId)
    {
        return TaskComment::where('task_id', $taskId)->get();
    }

    public function store(Request $request, $taskId)
    {
        $validatedData = $request->validate([
            'comment' => 'required|string',
        ]);

        $comment = TaskComment::create([
            'comment' => $validatedData['comment'],
            'user_id' => auth()->id(),
            'task_id' => $taskId,
        ]);

        return response()->json($comment, 201);
    }

    public function show($id)
    {
        $comment = TaskComment::findOrFail($id);
        return response()->json($comment);
    }

    public function update(Request $request,$taskId, $commentId)
    {
        $comment = TaskComment::findOrFail($commentId);

        $validate = Validator::make($request->all(), [
            "comment" => "required|string",
        ]);
        if ($validate->fails()) {
            return response()->json(['error' => $validate->errors()], 400);
        }

        $task = Task::findOrFail($taskId);
        $project = $task->project;

        // Check if user has permission to update this comment
        if ($comment->user_id !== Auth::id() && !$project->hasRole(Auth::id(), 'admin') && !$project->hasRole(Auth::id(), 'super_member')) {
            return response()->json(['error' => 'Forbidden'], 403);
        }

        $comment->update($request->all());

        return response()->json($comment);
    }

    public function destroy($taskId, $commentId)
    {
        $comment = TaskComment::findOrFail($commentId);
        $task = Task::findOrFail($taskId);
        $project = $task->project;

        // Check if user has permission to update this comment
        if ($comment->user_id !== Auth::id() && !$project->hasRole(Auth::id(), 'admin') && !$project->hasRole(Auth::id(), 'super_member')) {
            return response()->json(['error' => 'Forbidden'], 403);
        }

        $comment->delete();

        return response()->json(null, 204);
    }
}
