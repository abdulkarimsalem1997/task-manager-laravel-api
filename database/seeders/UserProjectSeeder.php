<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Project;
use Illuminate\Support\Facades\DB;


class UserProjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $users = User::all();
        $projects = Project::all();
        $users->each(function ($user) use ($projects) {
            $role = DB::table('roles')->where('name', 'member')->first();
            $user->projects()->attach(
                $projects->random(2)->pluck('id')->toArray(),
                [
                   
                    'role_id' => $role->id,
                    
                ]
            );
        });
        $this->command->info('Users attached to projects!');
    }
}
