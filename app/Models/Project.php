<?php

namespace App\Models;
use Spatie\Permission\Models\Role;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Project extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
        'user_id',
        
    ];

    public function users() : BelongsToMany

     {
        return $this->BelongsToMany(User::class,'user_project', 'project_id', 'user_id')->withPivot('role_id', 'status')->withTimestamps()->join('roles', 'user_project.role_id', '=', 'roles.id')
        ->select('users.*', 'roles.name as role', 'user_project.status');
    }

    public function tasks(): HasMany
     {
        return $this->hasMany(Task::class);
    }

    public function project_comments() : HasMany
    {
        return $this->hasMany(ProjectComment::class);
    }

    public function assignRole($userId, $roleName): void
    {
        $role =  Role::where('name', '=', $roleName)->first();
        $this->users()->updateExistingPivot($userId, ['role_id' => $role->id]);
    }

    public function admin()
    {
        return $this->belongsToMany(User::class)
            ->withPivot('role_id')
            ->wherePivot('role_id', '=', Role::where('name', 'admin')->first()->id);
    }

}
