<?php
namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ProjectInvitation extends Mailable
{
    use Queueable, SerializesModels;

    public $project;
    public $inviter;
    public $invitee;
    public $roleName;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($project, $inviter, $invitee ,$roleName)
    {
        $this->project = $project;
        $this->inviter = $inviter;
        $this->invitee = $invitee;
        $this->roleName = $roleName;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.project_invitation')
                    ->subject('Invitation to join project ' . $this->project->name)
                    ->with([
                        'projectName' => $this->project->name,
                        'inviterName' => $this->inviter->name,
                        'inviteeName' => $this->invitee->name,
                        'roleName' => 
                        $this->roleName,
                        'acceptLink' => route('projects.accept_invitation', ['project_id' => $this->project->id, 'invitee_id' => $this->invitee->id]),
                    ]);
    }
}
