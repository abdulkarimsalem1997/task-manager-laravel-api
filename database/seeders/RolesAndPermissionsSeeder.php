<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\PermissionRegistrar;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        app()[PermissionRegistrar::class]->forgetCachedPermissions();
        $arrayOfPermissionsNames = [
            'invite user',
            'delete user',
            'assign role',
            'edit project',
            'delete project',
            'create task',
            'delete task',
            'edit task',
            'delete project comment',
            'delete task comment',
        ];

        $permissions = collect($arrayOfPermissionsNames)->map(function ($permission) {
            return ['name' => $permission, 'guard_name' => 'api'];
        })->toArray();
        Permission::insert($permissions);


        Role::create(['name' => 'member', 'guard_name' => 'api']);
        Role::create(['name' => 'super_member', 'guard_name' => 'api'])->givePermissionTo([
            'invite user',
            'delete user',
            'create task',
            'edit task',
            'delete task',
            'delete project comment',
            'delete task comment'   
        ]);
        Role::create(['name' => 'admin', 'guard_name' => 'api'])->givePermissionTo($arrayOfPermissionsNames);

        $this->command->info('created roles and permissions');
    }
}
