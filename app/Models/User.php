<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable , HasRoles;

    protected $guard_name = 'api'; 

    
    public function projects() : BelongsToMany
    {
        return $this->BelongsToMany(Project::class, 'user_project', 'user_id', 'project_id')->withPivot('role_id', 'status')->withTimestamps() ->join('roles', 'user_project.role_id', '=', 'roles.id')
            ->join('users', 'user_project.user_id', '=', 'users.id')
            ->where('roles.name', '=', 'admin')
            ->select('projects.*', 'users.name as admin_name');
    }
    
    public function tasks(): HasMany
     {
        return $this->hasMany(Task::class);
    }

    public function project_comments() : HasMany
    {
        return $this->hasMany(ProjectComment::class);
    }

    public function task_comments() : HasMany
    {
        return $this->hasMany(TaskComment::class);
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'role',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
