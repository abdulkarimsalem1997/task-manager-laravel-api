<?php

namespace App\Http\Controllers;

use App\Models\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\ProjectComment;
use Exception;

class ProjectCommentController extends Controller
{
    public function __construct()
    {
        // Routes that admin and super_user only access
     
        $this->middleware('project_role:super_member,admin')->only(['inviteUser','removeUser', 'updateUserRole']);
    }

    public function index($projectId)
    {
        return ProjectComment::where('project_id', $projectId)->get();
    }

    public function store(Request $request, $projectId)
    {
        try{
        $validatedData = $request->validate([
            'comment' => 'required|string',
        ]);

        $comment = ProjectComment::create([
            'comment' => $validatedData['comment'],
            'user_id' => auth()->id(),
            'project_id' => $projectId,
        ]);

        return response()->json($comment, 201);

    } catch(Exception $e){
        return response()->json(['errors' => $e], 422);
    }
    }

    public function show($id)
    {
        $comment = ProjectComment::findOrFail($id);
        return response()->json($comment);
    }

    public function update(Request $request,  $projectId, $commentId)
    {
        $comment = ProjectComment::findOrFail($commentId);
        $project = Project::findOrFail($projectId);

        // Check if user has permission to update this comment
        if ($comment->user_id !== Auth::id() && !$project->hasRole(Auth::id(), 'admin') && !$project->hasRole(Auth::id(), 'super_member')) {
            return response()->json(['error' => 'Forbidden'], 403);
        }

        $comment->update($request->all());

        return response()->json($comment);
    }

    public function destroy($projectId, $commentId)
    {
        $comment = ProjectComment::findOrFail($commentId);
        $project = Project::findOrFail($projectId);
        
        // Check if user has permission to update this comment
        if ($comment->user_id !== Auth::id() && !$project->hasRole(Auth::id(), 'admin') && !$project->hasRole(Auth::id(), 'super_member')) {
            return response()->json(['error' => 'Forbidden'], 403);
        }

        $comment->delete();

        return response()->json(null, 204);
    }
}
