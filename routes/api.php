<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\TaskCommentController;
use App\Http\Controllers\ProjectCommentController;



// -----------------------Authentication------------------------

Route::post('/register', [AuthController::class, 'register']);
Route::post('/login', [AuthController::class, 'login']);

// ----------------------- Accept Invite ------------------------

Route::get('projects/{project_id}/accept/{invitee_id}', [ProjectController::class, 'acceptInvite'])->name('projects.accept_invitation');



// ----------------------- Sanctum Middleware ------------------------

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::middleware('auth:sanctum')->group(function () {

    //--------------------------- User routes -----------------------

    Route::get('user', [UserController::class, 'index']);
    Route::get('user/{user_id}', [UserController::class, 'show']);
    Route::get('user/projects', [UserController::class, 'projects']);
    Route::get('/user/tasks', [UserController::class, 'tasks']);

    //--------------------------- Project routes -----------------------

    Route::get('projects', [ProjectController::class, 'index']);
    Route::post('project', [ProjectController::class, 'store']);

    Route::get('project/{project_id}', [ProjectController::class, 'show']);
    Route::get('/projects/{project_id}/users', [ProjectController::class, 'showUsersInProject']);


    Route::put('project/{project_id}', [ProjectController::class, 'update']);
    Route::delete('project/{project_id}', [ProjectController::class, 'destroy']);

    
    Route::post('project/{project_id}/invite', [ProjectController::class, 'inviteUser']);

    Route::put('/projects/{project_id}/users/{user_id}/role', [ProjectController::class, 'updateUserRole']);
    Route::delete('/projects/{project_id}/users/{user_id}', [ProjectController::class, 'removeUser']);




    //--------------------------- Project comment routes ---------------- 

    Route::get('project/{project_id}/comments', [ProjectCommentController::class, 'index']);
    Route::post('project/{project_id}/comment', [ProjectCommentController::class, 'store']);
    Route::put('project/{project_id}/comment/{comment_id}', [ProjectCommentController::class, 'update']);
    Route::delete('project/{project_id}/comment/{comment_id}', [ProjectCommentController::class, 'destroy']);
    Route::get('project/comment/{comment_id}', [ProjectCommentController::class, 'show']);



    //--------------------------- Task routes ---------------------------

    Route::get('project/{project_id}/tasks', [TaskController::class, 'index']);


    Route::post('task/{project_id}', [TaskController::class, 'store']);
    Route::put('projects/{project_id}/tasks/{task_id}', [TaskController::class, 'update']);
    Route::delete('projects/{project_id}/tasks/{task_id}', [TaskController::class, 'destroy']);


    Route::get('task/{task_id}', [TaskController::class, 'show']);
    Route::get('projects/{project_id}/users/{user_id}/tasks', [TaskController::class, 'getUserTasksInProject']);
    Route::put('projects/{project_id}/tasks/{task_id}/status', [TaskController::class, 'updateStatus']);



    //--------------------------- Task comment routes ---------------------------

    Route::get('task/{task_id}/comments', [TaskCommentController::class, 'index']);
  
        Route::post('task/{task_id}/comment', [TaskCommentController::class, 'store']);
        Route::put('task/{task_id}/comment/{comment_id}', [TaskCommentController::class, 'update']);
        Route::delete('task/{task_id}/comment/{comment_id}', [TaskCommentController::class, 'destroy']);
 
    Route::get('task/comment/{comment_id}', [TaskCommentController::class, 'show']);
});
