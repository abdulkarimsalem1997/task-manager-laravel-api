<?php

namespace App\Http\Controllers;

use Illuminate\Validation\ValidationException;
use Illuminate\Http\Request;
use App\Models\Task;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Rules\IsUserInProject;
use Illuminate\Support\Facades\Validator;

class TaskController extends Controller
{

    public function __construct()
    {
        $this->middleware('project_role:super_member,admin')->only(['store', 'update', 'destroy']);  
        $this->middleware('project_role:super_member,admin,member')->only(['show', 'index', 'getUserTasksInProject', 'updateStatus']);
    }

    public function index($projectId)
    {
        $tasks = Task::where('project_id', $projectId)->get();
        return response()->json($tasks);
    }

    public function store(Request $request , $projectId)
    {  
        try{

      
        $validatedData = $request->validate([
            // 'project_id' => 'required|exists:projects,id',
            // 'user_id' => 'required|exists:users,id',
            'title' => 'required|string|max:255',
            'description' => 'nullable|string',
            'status' => 'required|string|in:queue,in progress,done',
            'due_date' => 'required|date',
        ]);

       
        $task = Task::create([
            'title' => $validatedData['title'],
            'description' => $validatedData['description'],
            'status' => $validatedData['status'],
            'due_date' => $validatedData['due_date'],
            'project_id' => $projectId,
            'user_id' => auth()->id(),
        ]);

        return response()->json($task, 201);
    }catch (ValidationException $e) {
        return response()->json(['errors' => $e->errors()], 422);
    }
    }

    public function show($id)
    {
        $task = Task::findOrFail($id);
        return response()->json($task);
    }

    public function getUserTasksInProject($projectId, $userId)
    {
        

        $validate = Validator::make([
            'project_id' => $projectId,
            'user_id' => $userId
        ], 
            [
            'project_id' => 'required|exists:projects,id',
            'user_id' => ['required', 'exists:users,id',
             new IsUserInProject($projectId)],
        ]);
       
        if ($validate->fails()) {
            return response()->json([
                'error' => $validate->errors(),
            ], 400);
        }

       
        $tasks = Task::where('project_id', $projectId)
            ->where('user_id', $userId)
            ->get();

        return response()->json($tasks);
    }

    // note: put projectId for CheckProjectRole class
    public function update(Request $request, $projectId, $taskId)
    {
        $task = Task::findOrFail($taskId);

        $validatedData = $request->validate([
            'title' => 'required|string|max:255',
            'description' => 'nullable|string',
            'status' => 'required|in:queue,in_progress,done',
            'due_date' => 'nullable|date',
        ]);

        $task->update($validatedData);

        return response()->json($task);
    }
    // note: put projectId for CheckProjectRole class
    public function updateStatus(Request $request, $projectId, $taskId)
    {
        $validated = Validator::make($request->all(), [
            'status' => 'required|string|in:in queue,in progress,done'
        ]);

        if ($validated->fails()) {
            return response()->json(['errors' => $validated->errors()], 400);
        }
        
        $user = Auth::user();
        
        $task = Task::findOrFail($taskId);
        
        if ($task->user_id !== $user->id) {
            return response()->json(['error' => 'Forbidden'], 403);
        }

        $task->update(['status' => $validated->validated()['status']]);

        return response()->json($task);
    }

    // note: put projectId for CheckProjectRole class
    public function destroy( $projectId, $taskId)
    {   
        $task = Task::findOrFail($taskId);
      
        $task->delete();

        return response()->json(null, 204);
    }
}
